## The GitLab Directory

The purpose of this directory is to serve as a place to store
GitLab Manifests that helps drive your software development life
cycle from Idea through Production.

* [Default Secret Detection Rules](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml)
* [How we Dogfood CODEOWNERS](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/CODEOWNERS)


#### Resources

* [Spreadsheet to copy/paste into Issue](https://docs.google.com/spreadsheets/d/1bkNedYMhhMMRUs-PfWsejwmzVQou5ID5Ze8H9Rczr7o/edit)
